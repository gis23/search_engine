Docker-host создается на GCE через `docker-machine`

for linux:
```
docker-machine create --driver google \
--google-project ninth-arena-182211 \
--google-machine-image https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/family/ubuntu-1604-lts \
--google-machine-type n1-standard-1 \
--google-zone europe-west1-b \
--google-open-port 5601/tcp \
--google-open-port 9292/tcp \
--google-open-port 9411/tcp \
vm1
```

for windows:
```
docker-machine create --driver google `
    --google-project ninth-arena-182211 `
    --google-machine-image https://www.googleapis.com/compute/v1/projects/ubuntu-os-cloud/global/images/family/ubuntu-1604-lts `
    --google-machine-type n1-standard-1 `
    --google-zone europe-west1-b `
    --google-open-port 80/tcp `
    --google-open-port 3000/tcp `
    --google-open-port 8000/tcp `
    --google-open-port 8080/tcp `
    --google-open-port 9090/tcp `
    --google-open-port 9093/tcp `
vm1
```

Затем

```
eval $(docker-machine vm1)
```
или docker-machine use vm1

после чего запускаем docker-compose

```
docker-compose up -d
```
Причем crawler нужно запустить повторно
он отваливает если не может подключиться к монго.

```
docker-compose ps
            Name                          Command               State                     Ports
----------------------------------------------------------------------------------------------------------------
searchengine_alertmanager_1    /bin/alertmanager -config. ...   Exit 1
searchengine_cadvisor_1        /usr/bin/cadvisor -logtostderr   Up       0.0.0.0:8080->8080/tcp
searchengine_crawler_1         python -u crawler.py https ...   Up       0.0.0.0:8081->8081/tcp
searchengine_grafana_1         /run.sh                          Up       0.0.0.0:3000->3000/tcp
searchengine_mongo_1           docker-entrypoint.sh mongod      Up       27017/tcp
searchengine_node-exporter_1   /bin/node_exporter --path. ...   Up       9100/tcp
searchengine_prometheus_1      /bin/prometheus --config.f ...   Up       0.0.0.0:9090->9090/tcp
searchengine_rabbit_1          docker-entrypoint.sh rabbi ...   Up       25672/tcp, 4369/tcp, 5671/tcp, 5672/tcp
searchengine_ui_1              gunicorn ui:app -b 0.0.0.0       Up       0.0.0.0:8000->8000/tcp
```

внешний адрес своего докер хоста привязал на `http://gitlab.devex.pro`
UI поисковика соответсвенно доступен по `http://gitlab.devex.pro:8000`
Prometheus `http://gitlab.devex.pro:9090`
cAdvisor 'http://gitlab.devex.pro:8080'
Grafana 'http://gitlab.devex.pro:3000'

Не получилось сделать деплой через gitlab-ci.
Не разобрался как можно через gitlab-ci готовые образы запустить на docker хосте на GCE.
Еще затык был при попытке собирать docker образа на самостоятельно поднятом gitlab-ci какая-то проблема с docker in docker
